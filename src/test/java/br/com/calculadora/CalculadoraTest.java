package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void inicializa(){
        calculadora = new Calculadora();
    }

    @Test
    public void testarSomaDeDoisNumerosInteiros(){
//        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.somar(1, 2);

        Assertions.assertEquals(3, resultado);

    }

    @Test
    public void testarSomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.somar(2.3, 3.4);

        Assertions.assertEquals(5.7, resultado);

    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosInteiros(){
        int resultado =  calculadora.multiplicar(2, 2);

        Assertions.assertEquals(4,resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosNegativos(){
        int resultado =  calculadora.multiplicar(-2, -2);

        Assertions.assertEquals(4,resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosSendoUmNegativo(){
        int resultado =  calculadora.multiplicar(-2, 2);

        Assertions.assertEquals(-4,resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosFlutuantes(){
        double resultado =  calculadora.multiplicar(2.5, 2.5);

        Assertions.assertEquals(6.25,resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosNegativosFlutuantes(){
        double resultado =  calculadora.multiplicar(-2.5, -2.5);

        Assertions.assertEquals(6.25,resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosFlutuantesSendouUmNegativo(){
        double resultado =  calculadora.multiplicar(2.5, -2.5);

        Assertions.assertEquals(-6.25,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteiros(){
        int resultado =  calculadora.dividir(4, 2);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteirosNegativos(){
        int resultado =  calculadora.dividir(-4, -2);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosInteirosSendoUmNegativo(){
        int resultado =  calculadora.dividir(-4, 2);

        Assertions.assertEquals(-2,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosFlutuantes(){
        double resultado =  calculadora.dividir(4.5, 2.5);

        Assertions.assertEquals(1.8,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosFlutuantesNegativos(){
        double resultado =  calculadora.dividir(-4.5, -2.5);

        Assertions.assertEquals(1.8,resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosFlutuantesSendoUmNegativo(){
        double resultado =  calculadora.dividir(-4.5, 2.5);

        Assertions.assertEquals(-1.8,resultado);
    }
}
